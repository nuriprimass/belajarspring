package com.primas.springmvc.service;

import com.primas.springmvc.dao.MahasiswaDao;
import com.primas.springmvc.entity.Mahasiswa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MahasiswaService implements BaseService<Mahasiswa>{
    @Autowired
    private MahasiswaDao mahasiswaDao;


    public Mahasiswa save(Mahasiswa entity) {
        entity = mahasiswaDao.save(entity);
        return entity;
    }

    public Mahasiswa update(Mahasiswa entity) {
        entity = mahasiswaDao.update(entity);
        return entity;
    }

    public Mahasiswa delete(long id) {
        return null;
    }

    public List<Mahasiswa> findAll() {

        return null;
    }

    public Mahasiswa findById(long id) {
        return null;
    }
}
