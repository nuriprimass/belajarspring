package com.primas.springmvc.service;

import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface BaseService<T> {

    T save(T entity);

    T update (T entity);

    List<T> findAll();

    T findById(long id);

    T delete(long id);
}
