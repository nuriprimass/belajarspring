package com.primas.springmvc.controller;

import com.sun.rowset.internal.Row;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class BaseController {
    protected Locale ID = new Locale("id");

    //Ini adalah kemungkinan struktur data yang ada
    protected Map<String, Object> convertModel(Object data, Object status){
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("data", data);
        model.put("status", status);
        return model;
    }
    protected Map<String, Object> convertModel(Object data, Object rows, Object status){
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("data", data);
        model.put("rows", rows);
        model.put("status",status);
        return model;
    }
    protected Map<String, Object> convertModel(Object data) {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("data", data);
        return model;
    }

    protected Map<String, Object> convertModel(String status) {
        Map<String, Object> model = new HashMap();
        model.put("status", status);
        return model;
    }
}
