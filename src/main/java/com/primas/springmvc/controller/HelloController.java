package com.primas.springmvc.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
@RestController
public class HelloController extends BaseController{
    @GetMapping("/")
    public Map<String, Object> helloWord() {
        return convertModel("Bila Muncul Ini berarti Hello World", HttpStatus.OK);
    }
}
