package com.primas.springmvc.dao;

import com.primas.springmvc.entity.Mahasiswa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MahasiswaDaoImpl implements MahasiswaDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Mahasiswa save(Mahasiswa entity){
        String SQLINSERT = "INSERT INTO mahasiswa values(?,?,?)";
        List<Object> params = new ArrayList<Object>();
        params.add(entity.getNim());
        params.add(entity.getNama());
        params.add(entity.getAlamat());

        jdbcTemplate.update(SQLINSERT, params);

        return entity;
    }
    public Mahasiswa update(Mahasiswa entity){
        String SQLUPDATE = "UPDATE mahasiswa SET nim=?,nama=?,alamat=? WHERE id=?";
        List<Object> params= new ArrayList<Object>();
        params.add(entity.getNim());
        params.add(entity.getNama());
        params.add(entity.getAlamat());
        params.add(entity.getId());
        jdbcTemplate.update(SQLUPDATE, params);

        return entity;
    }
    public Mahasiswa delete(long id) {
        String SQLDELETE = "DELETE FROM mahasiswa WHERE id=?";
        List<Object> params= new ArrayList<Object>();
        params.add(id);
        jdbcTemplate.update(SQLDELETE);

        return null;
    }
    public List<Mahasiswa> findAll(){
        String sql = "SELECT * FROM MAHASISWA ";

        return jdbcTemplate.query(sql, new RowMapper<Mahasiswa>() {
            public Mahasiswa mapRow(ResultSet resultSet, int i) throws SQLException {
                Mahasiswa mahasiswa = new Mahasiswa();
                mahasiswa.setId(resultSet.getLong("id"));
                mahasiswa.setNim(resultSet.getString("nim"));
                mahasiswa.setNama(resultSet.getString("nama"));
                mahasiswa.setAlamat(resultSet.getString("alamat"));
                return mahasiswa;
            }
        });
    }
    public Mahasiswa findById(long id) {
        return null;
    }
}
