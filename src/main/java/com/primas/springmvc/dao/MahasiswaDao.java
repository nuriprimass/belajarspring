package com.primas.springmvc.dao;

import com.primas.springmvc.entity.Mahasiswa;

public interface MahasiswaDao extends BaseDao<Mahasiswa>{
}
