package com.primas.springmvc.dao;

import java.util.List;

public interface BaseDao<T> {
    T save (T entity);

    T update (T entity);

    List<T> findAll();

    T findById(long id);

    T delete(long id);

}
