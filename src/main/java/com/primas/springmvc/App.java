package com.primas.springmvc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
    public static Logger getLogger(Object o) {
        return LoggerFactory.getLogger(o.getClass());
    }
}
